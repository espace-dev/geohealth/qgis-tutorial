# Preface {.unnumbered}

This training is organized by the French National Research Institute for Sustainable Development (IRD), in the frame of **Veterinary Entomology in Cambodia (VECAM) project**. This project is a collaboration between the General Directorate of Animal Health and Production (GDAHP), the Royal University of Agriculture (RUA) and the Institut Pasteur du Cambodge (IPC).

**The FSPI (Solidarity Fund for Innovative Projects) VECAM is funded by the French Ministry for Europe and Foreign Affairs**

**Training given by**:

-   Dr. Vincent Herbreteau, Researcher in Health Geography at IRD-Espace-Dev, Phnom Penh, Cambodia, vincent.herbreteau\@ird.fr
-   Sokeang Hoeun, Geomatician in the IRD-IPC GeoHealth Team, shoeun\@pasteur-kh.org
-   Lucas Longour, Geomatician at IRD-Espace-Dev, Phnom Penh, Cambodia, lucas.longour\@ird.fr

**Location**: at the Royal University of Agriculture (RUA), Faculty of Veterinary Medicine, Samdach Techo Hun Sen Building, Room A06

**Program**:

![](img/schedule_vecam.png){fig-align="center"}

## Past sessions

-   May 26th - 31st 2019, at Institut Pasteur du Cambodge, Phnom Penh for the Ecomore 2 Project. Course given by Vincent Herbreteau and Arnaud Vandecasteele
